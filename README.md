![Screen Shot 2016-09-16 at 5.06.47 PM.png](https://bitbucket.org/repo/GAqd8y/images/2236123334-Screen%20Shot%202016-09-16%20at%205.06.47%20PM.png)

# README #

Hi, welcome to my bucket of RS and GIS scripts. Please, take what you like!

### What is this repository for? ###

* This repository is used to host the scripts that I have written as part of my PhD/Post-Doc in remote sensing. Occasionally they find their way onto a great blog: [SpectralDifferences](http://spectraldifferences.wordpress.com). If you're in the field of geospatial information and analysis and you would like to use something that I have here, then please feel free to help yourself.


### Me ###

* Im Nathan, a ESSIC UMD PDRA at NASA's Goddard Space Flight Center. My research revolves around investigating a combination of remotely sensed data for land cover mapping and monitoring (particularly wetlands). 

* Alternatively, you can follow [me](https://twitter.com/Nmt28) or my university research group (from which I recently graduated) [EOEDL](https://twitter.com/AU_EarthObs) on Twitter.

* If you wish to contact me you can do so via nmthomas28@gmail.com
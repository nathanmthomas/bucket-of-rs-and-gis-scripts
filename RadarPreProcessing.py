#! /usr/bin/env python

''' Author: Nathan Thomas
    Email: nmthomas28@gmail.com
    Date: 10/23/2014
    Version: 1.0
    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'''


import os.path
import sys
import glob
import argparse
import rsgislib
from rsgislib import tools
import numpy


def RadarProcess(args, R, C, TC, LF, DB, W, PS, F):
    # Each node needs to reference the node before it ('<sourceProduct refid=/>')
    # so that it is able to process the scene in a chain and so the script
    # knows which order to run in. Storing the node names enables this
    NodeList = []

    # Set the output XML file and prepare to write
    outXML=args.xmlfile
    outfile = open(outXML, 'w+')

    # READ PARAMETERS
    inRadar = args.input
    ################
    
    
    # READ FUNCTION
    if R == True:
        NodeList.append("Read")
        outfile.write("""<graph id="Graph">
  <version>1.0</version>
  <node id="Read">
    <operator>Read</operator>
    <sources/>
    <parameters class="com.bc.ceres.binding.dom.XppDomElement">
        <file>""" + str(inRadar) + """</file>
    </parameters>
  </node>""")
    #############
      
      
    # CALIBRATE
    if C == True:
        outfile.write("""
  <node id="Calibration">
    <operator>Calibration</operator>
    <sources>
      <sourceProduct refid=\"""" + NodeList[-1] + """\"/>
  </sources>
  <parameters class="com.bc.ceres.binding.dom.XppDomElement">
      <sourceBands/>
      <auxFile>Product Auxiliary File</auxFile>
      <externalAuxFile/>
      <outputImageInComplex>false</outputImageInComplex>
      <outputImageScaleInDb>false</outputImageScaleInDb>
      <createGammaBand>false</createGammaBand>
      <createBetaBand>false</createBetaBand>
      <selectedPolarisations/>
      <outputSigmaBand>true</outputSigmaBand>
      <outputGammaBand>false</outputGammaBand>
      <outputBetaBand>false</outputBetaBand>
    </parameters>
  </node>""")
        NodeList.append("Calibration")
    ########################



    # RANGE-DOPPLER TERRAIN CORRECTION

    if TC==True:
        # Calculate the pixel spacing in degrees based on the pixel size provided in m
        ellipse = [6378137.0, 6356752.314245]
        radlat = numpy.deg2rad(0)
        Rsq = (ellipse[0]*numpy.cos(radlat))**2+(ellipse[1]*numpy.sin(radlat))**2
        Mlat = (ellipse[0]*ellipse[1])**2/(Rsq**1.5)
        Nlon = ellipse[0]**2/numpy.sqrt(Rsq)
        pixelSpacingInDegreeX = PS / (numpy.pi/180*numpy.cos(radlat)*Nlon)
        pixelSpacingInDegreeY = PS / (numpy.pi/180*Mlat)
        outfile.write("""
  <node id="Terrain-Correction">
    <operator>Terrain-Correction</operator>
    <sources>
      <sourceProduct refid=\"""" + NodeList[-1] + """\"/>
    </sources>
    <parameters class="com.bc.ceres.binding.dom.XppDomElement">
      <sourceBands/>
      <demName>SRTM 3Sec</demName>
      <externalDEMFile/>
      <externalDEMNoDataValue>0.0</externalDEMNoDataValue>
      <externalDEMApplyEGM>True</externalDEMApplyEGM>
      <demResamplingMethod>BILINEAR_INTERPOLATION</demResamplingMethod>
      <imgResamplingMethod>BILINEAR_INTERPOLATION</imgResamplingMethod>
      <pixelSpacingInMeter>""" + str(PS) + """</pixelSpacingInMeter>
      <pixelSpacingInDegree>""" + str(pixelSpacingInDegreeX) + """</pixelSpacingInDegree>
      <mapProjection>GEOGCS[&quot;WGS84(DD)&quot;,
  DATUM[&quot;WGS84&quot;,
    SPHEROID[&quot;WGS84&quot;, 6378137.0, 298.257223563]],
  PRIMEM[&quot;Greenwich&quot;, 0.0],
  UNIT[&quot;degree&quot;, 0.017453292519943295],
  AXIS[&quot;Geodetic longitude&quot;, EAST],
  AXIS[&quot;Geodetic latitude&quot;, NORTH]]</mapProjection>
      <nodataValueAtSea>False</nodataValueAtSea>
      <saveDEM>False</saveDEM>
      <saveLatLon>False</saveLatLon>
      <saveIncidenceAngleFromEllipsoid>False</saveIncidenceAngleFromEllipsoid>
      <saveLocalIncidenceAngle>False</saveLocalIncidenceAngle>
      <saveProjectedLocalIncidenceAngle>False</saveProjectedLocalIncidenceAngle>
      <saveSelectedSourceBand>True</saveSelectedSourceBand>
      <outputComplex>False</outputComplex>
      <applyRadiometricNormalization>False</applyRadiometricNormalization>
      <saveSigmaNought>False</saveSigmaNought>
      <saveGammaNought>False</saveGammaNought>
      <saveBetaNought>False</saveBetaNought>
      <incidenceAngleForSigma0>Use projected local incidence angle from DEM</incidenceAngleForSigma0>
      <incidenceAngleForGamma0>Use projected local incidence angle from DEM</incidenceAngleForGamma0>
      <auxFile>Latest Auxiliary File</auxFile>
      <externalAuxFile/>
    </parameters>
  </node>""")
        NodeList.append("Terrain-Correction")
    ############################
    
    # LEE
    if LF==True:
        outfile.write("""
  <node id="Speckle-Filter">
    <operator>Speckle-Filter</operator>
    <sources>
      <sourceProduct refid=\"""" + NodeList[-1] + """\"/>
    </sources>
    <parameters class="com.bc.ceres.binding.dom.XppDomElement">
      <sourceBands/>
      <filter>Lee Sigma</filter>
      <filterSizeX>3</filterSizeX>
      <filterSizeY>3</filterSizeY>
      <dampingFactor>2</dampingFactor>
      <estimateENL>true</estimateENL>
      <enl>1.0</enl>
      <numLooksStr>1</numLooksStr>
      <windowSize>7x7</windowSize>
      <targetWindowSizeStr>3x3</targetWindowSizeStr>
      <sigmaStr>0.9</sigmaStr>
      <anSize>50</anSize>
    </parameters>
  </node>""")
        NodeList.append("Speckle-Filter")
    #############################
    
    # Linear to dB
    if DB==True:
        outfile.write("""
  <node id="LinearToFromdB">
    <operator>LinearToFromdB</operator>
    <sources>
      <sourceProduct refid=\"""" + NodeList[-1] + """\"/>
    </sources>
    <parameters class="com.bc.ceres.binding.dom.XppDomElement">
      <sourceBands/>
    </parameters>
  </node>""")
        NodeList.append("LinearToFromdB")
    ###############################

    # WRITE
    if W==True:
        outfile.write("""
  <node id="Write">
    <operator>Write</operator>
    <sources>
      <sourceProduct refid=\"""" + NodeList[-1] + """\"/>
    </sources>
    <parameters class="com.bc.ceres.binding.dom.XppDomElement">
      <file>""" + args.output + """</file>
      <formatName>""" + F + """</formatName>
    </parameters>
  </node>""")
    #########################

    # OTHER XML REQUIREMENTS
    outfile.write("""
  <applicationData id="Presentation">
    <Description/>
    <node id="Read">
        <displayPosition x="37.0" y="134.0"/>
    </node>""")
    if C == True:
        outfile.write("""
    <node id="Calibration">
        <displayPosition x="170.0" y="135.0"/>
    </node>""")
    if TC == True:
        outfile.write("""
    <node id="Terrain-Correction">
        <displayPosition x="284.0" y="128.0"/>
    </node>""")
    if LF == True:
        outfile.write("""
    <node id="Speckle-Filter">
        <displayPosition x="211.0" y="154.0"/>
    </node>""")
    outfile.write("""
    <node id="Write">
        <displayPosition x="455.0" y="135.0"/>
    </node>
  </applicationData>
</graph>""")




def main():
    print("N Thomas 2018. THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\nUSE '-h' FOR HELP AND INPUT PARAMETERS. DIFFERENT LEVELS OF PROCESSING CAN BE SPECIFIED UNDER 'EDIT ME'. USAGE: 'python RadarPreProcessing.py -i /SentinelFile.SAFE -o /Sentinel1_Cal.tif -x /XML2RUN.xml.\nOUTPUT XML CAN BE RUN USING '/APPLICATIONS/SNAP/BIN/GPT XML2RUN.XML'\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, help="Specify the input RADAR file (.SAFE for Sentinel)")
    parser.add_argument("-o", "--output", type=str, help="Specify the desired output Calibrated filename to be created")
    parser.add_argument("-x", "--xmlfile", type=str, help="Specify the output XML name to be created")
    # Add in args to specify pixel size and file format rather than hardcode
    args = parser.parse_args()
    
    if args.input==None:
        print("INPUT RADAR FILEPATH NOT PROVIDED - REFER TO USAGE ABOVE\n")
        os._exit(1)
    if args.output==None:
        print("OUTPUT FILEPATH NOT PROVIDED - REFER TO USAGE ABOVE\n")
        os._exit(1)
    if args.xmlfile==None:
        print("OUTPUT XML FILEPATH NOT PROVIDED - REFER TO USAGE ABOVE\n")
        os._exit(1)

    ############ EDIT ME ###############
    read=True
    Calibrate=True
    TerrainCorrection=True
    LeeFilter=False
    lin2dB=True
    Write=True
    PixelSizeInMetres=10
    Format = 'ENVI'
    ###################################


    if read==True:
        print("Read = True\n Reading in " + str(args.input) + "\n")
    if Calibrate==True:
        print("Calibrate = True\n Additional output files (e.g. Gamma Band) can be specified in function\n")
    if TerrainCorrection==True:
        print("Terrain correction = True\n Additional output files (e.g. Local Incidence Angle) can be specified in function\n")
    if LeeFilter==True:
        print("Lee Filter = True\n Specifics (Window Size) can be specified in function\n")
    if lin2dB==True:
        print("lin2dB = True\n Will Output in dB \n")
    if Write==True:
        print("Write = True\n OutXML = " + str(args.xmlfile) + "\n" + "Outfile = " + str(args.output) + "\n")



    RadarProcess(args, read, Calibrate, TerrainCorrection, LeeFilter, lin2dB, Write, PixelSizeInMetres, Format)


if __name__ == "__main__":
    main()

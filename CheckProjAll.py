import subprocess
import argparse
import os.path

def CheckProjection(args):
    
    for i in range(len(args.inputs)):
        projection = ' '.join(args.proj)
        check = subprocess.getstatusoutput('gdalinfo ' + str(args.inputs[i]))
        info = check[1]
        projline = (info.split('\n')[4].split('"')[1])
    
        if args.proj is None:
            print('FILE: ' + str(args.inputs[i]) + ' PROJECTION IS: ' + str(projline) + '\n')

        elif str(projection) == str(projline):
            print('Projections Match: ' + str(projline))
            pass
        elif str(projection) != str(projline):
            print('WARNING ' + str(i+1) + ' of ' + str(len(args.inputs)))
            print('PROJECTIONS DO NOT MATCH\n')
            print('FILE: ' + str(args.inputs[i]) + ' PROJECTION IS: ' + str(projline) + '\n')
            print('DESIRED PROJECTION IS: ' + "'" + str(projection) + "'" + '\n')


#os._exit(1)











def main():
    print("'CheckProj.py' WAS WRITTEN BY NATHAN THOMAS OF THE EARTH OBSERVATION AND ECOSYSTEMS DYNAMICS LABORATORY, ABERYSTWYTH UNIVERSITY. IT IS USED TO CHECK THE PROJECTIONS FROM A LIST OF INPUT IMAGES\n")
    parser = argparse.ArgumentParser()
    parser.add_argument('inputs', type=str, nargs='+')
    parser.add_argument('-p', '--proj', type=str, nargs='+')


    args = parser.parse_args()
    
    
    
    CheckProjection(args)


if __name__ == "__main__":
    main()
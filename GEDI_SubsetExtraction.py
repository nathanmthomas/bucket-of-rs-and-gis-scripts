import h5py
import numpy as np
import pandas as pd
import subprocess
import matplotlib
import matplotlib.pyplot as plt
import os
import ogr

# Folder to H5s
inDir = '/path/to/H5s/'

files = [file for file in os.listdir(inDir) if file.endswith('.h5')]

# Output Folder
outDir = '/Me/MyData/GEDI/'

polygon = '/A/Shapefile/of/my/ROI.shp'
inDriver = ogr.GetDriverByName("ESRI Shapefile")

# Get Polygon Geometry (bbox)
inDataSource = inDriver.Open(polygon, 1)
inLayer = inDataSource.GetLayer()
for feature in inLayer:
    geom=feature.GetGeometryRef().GetEnvelope()


# Bounding box of interest
maxy = float(geom[3])
minx = float(geom[0])
miny = float(geom[2])
maxx = float(geom[1])

for file in files:

    H5 = os.path.join(inDir, file)
    f = h5py.File(H5,'r')

    # Set the names of the 6 lasers
    lines = ['BEAM0000', 'BEAM0001', 'BEAM0010', 'BEAM0011', 'BEAM0101', 'BEAM0110', 'BEAM1000', 'BEAM1011']
    
    # set up blank lists
    # Can easily add more in based on metrics in H5 file
    latitude = []
    longitude =[]
    RH100 = []
    Quality = []

    for line in lines:
        print('line =', line)
        RH100.append(f['/' + line    + '/rh100/'][...,].tolist())
        Quality.append(f['/' + line    + '/l2b_quality_flag/'][...,].tolist())
        latitude.append(f['/' + line    + '/geolocation/lat_lowestmode/'][...,].tolist())
        longitude.append(f['/' + line   + '/geolocation/lon_lowestmode/'][...,].tolist())

    # IceSat-2 lasers do not always have the same number of data points as each other
    # Not sure if GEDI is the same so this is borrowed from some ICESat-2 code I wrote
    # Appends the lists into one array for subsequent processing/analysis
    latitude=np.array([latitude[l][k] for l in range(8) for k in range(len(latitude[l]))] )
    longitude=np.array([longitude[l][k] for l in range(8) for k in range(len(longitude[l]))] )
    RH100=np.array([RH100[l][k] for l in range(8) for k in range(len(RH100[l]))] )
    Quality=np.array([Quality[l][k] for l in range(8) for k in range(len(Quality[l]))] )

    print(np.shape(RH100))

    # Create a binary array of which points are in bbox
    SubsetLat = np.where(((latitude<maxy) & (longitude<maxx) & (latitude>miny) & (longitude>minx)), latitude, -9999)
    SubsetLon = np.where(((latitude<maxy) & (longitude<maxx) & (latitude>miny) & (longitude>minx)), longitude, -9999)
    SubsetRH100 = np.where(((latitude<maxy) & (longitude<maxx) & (latitude>miny) & (longitude>minx)), RH100, -9999)
    SubsetQuality = np.where(((latitude<maxy) & (longitude<maxx) & (latitude>miny) & (longitude>minx)), Quality, -9999)

    # Subset Data based whether it is in the bbox
    SubsetLat = SubsetLat[SubsetLat!=-9999]
    SubsetLon = SubsetLon[SubsetLon!=-9999]
    SubsetRH100 = SubsetRH100[SubsetRH100!=-9999]
    SubsetQuality = SubsetQuality[SubsetQuality!=-9999]

    # Only include data that has a high-quality flag
    SubsetLat = SubsetLat[SubsetQuality==1]
    SubsetLon = SubsetLon[SubsetQuality==1]
    SubsetRH100 = SubsetRH100[SubsetQuality==1]
    SubsetQuality = SubsetQuality[SubsetQuality==1]

    # Write out to a CSV
    out=pd.DataFrame(
                     {'X': SubsetLon,
                     'Y': SubsetLat,
                     'RH100':SubsetRH100
                     })
        
    # Write out to a csv
    print('Creating CSV...')
    filename = file.split('.')[0]
    out.to_csv(os.path.join(outDir, filename + '.csv'),index=False, encoding="utf-8-sig")

    # Convert CSV to shp
    print('ogr2ogr -s_srs EPSG:4326 -t_srs EPSG:4326 -oo X_POSSIBLE_NAMES=X -oo Y_POSSIBLE_NAMES=Y  -f "ESRI Shapefile" ' + os.path.join(outDir, filename + '.shp') + ' ' +  os.path.join(outDir, filename + '.csv'))
    subprocess.call('ogr2ogr -s_srs EPSG:4326 -t_srs EPSG:4326 -oo X_POSSIBLE_NAMES=X -oo Y_POSSIBLE_NAMES=Y  -f "ESRI Shapefile" ' + os.path.join(outDir, filename + '.shp') + ' ' +  os.path.join(outDir, filename + '.csv'), shell=True)


